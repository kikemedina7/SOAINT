package model;

public class DisabledComponents {
    
    private boolean buttons;
    private boolean table;
    private boolean columns;
    
    public DisabledComponents() {
        super();
    }


    public DisabledComponents(boolean buttons, boolean table, boolean columns) {
        this.buttons = buttons;
        this.table = table;
        this.columns = columns;
    }

    public void setButtons(boolean buttons) {
        this.buttons = buttons;
    }

    public boolean isButtons() {
        return buttons;
    }

    public void setTable(boolean table) {
        this.table = table;
    }

    public boolean isTable() {
        return table;
    }

    public void setColumns(boolean columns) {
        this.columns = columns;
    }

    public boolean isColumns() {
        return columns;
    }

}
