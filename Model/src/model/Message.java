package model;

public class Message {
    
    public static final String ERROR = "Error al contactar el WS!";    
    public static final String SUCCESS = "Se ha conectado exitosamente al contactar el WS!";
    
    public Message() {
        super();
    }
    
    public static String messageError(){
        return ERROR;
    }
    
    public static String messageSuccess(){
        return SUCCESS;
    }
    
    
}
