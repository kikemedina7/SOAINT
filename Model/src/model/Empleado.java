package model;

import java.io.Serializable;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;


public class Empleado implements Serializable{
    @SuppressWarnings("compatibility:-7924938839084615812")
    private static final long serialVersionUID = 1L;


    private int numeroIdentificacion;
    private String nombres;
    private String apellidos;
    private Integer edad;
    private Date fechaRegistro;
    
    public Empleado() {
        super();
    }


    public Empleado(int numeroIdentificacion, String nombres, String apellidos, Integer edad, Date fechaRegistro) {
        this.numeroIdentificacion = numeroIdentificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.fechaRegistro = fechaRegistro;
    }

    public void setNumeroIdentificacion(int numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public int getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getNombres() {
        return nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

}
