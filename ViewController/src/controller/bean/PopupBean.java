package controller.bean;


import libraries.JSFUtil;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.DialogEvent.*;

public class PopupBean {
    public PopupBean() {
    }

    public void proccessDialog(DialogEvent dialogEvent) {
        // Add event code here...
        Outcome out = dialogEvent.getOutcome();
        if(out == out.yes){
            JSFUtil.addInfoMessage("Has presionado YES");
        }else if(out == out.no){
            JSFUtil.addInfoMessage("Has presionado NO");
        }
    }
}
