package controller.bean;

import javax.faces.event.ActionEvent;

import libraries.JSFUtil;

import model.*;
public class DisabledTabledBean {
    
    private DisabledComponents disComponents;
    
    public DisabledTabledBean() {
        super();
        this.disComponents = (DisabledComponents) JSFUtil.resolveExpression("#{pageFlowScope.IndexBean.disComponents}");
    }

    public void setDisComponents(DisabledComponents disComponents) {
        this.disComponents = disComponents;
    }

    public DisabledComponents getDisComponents() {
        return disComponents;
    }
    
    public void retornarVista(ActionEvent event){
        this.disComponents.setButtons(Boolean.FALSE);
        this.disComponents.setColumns(Boolean.FALSE);
        this.disComponents.setTable(Boolean.FALSE);
    }

}
