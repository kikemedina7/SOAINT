package controller.bean;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import libraries.JSFUtil;

import model.DisabledComponents;
import model.Empleado;
import tools.ShowHideComponents;

public class IndexBean implements Serializable {
    @SuppressWarnings("compatibility:-8766728095136521321")
    private static final long serialVersionUID = 204708409869962147L;

    private int size;
    private List<Empleado> listaEmpleado;
    private ShowHideComponents componentVisible;
    private ShowHideComponents showHideButtons;
    private DisabledComponents disComponents;
    
    public IndexBean() {
        super();
       this.listaEmpleado = new ArrayList<Empleado>();
       this.componentVisible = new ShowHideComponents();
       this.showHideButtons = new ShowHideComponents();
       this.disComponents = new DisabledComponents(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
    }


    public void setListaEmpleado(List<Empleado> listaEmpleado) {
        this.listaEmpleado = listaEmpleado;
    }

    public List<Empleado> getListaEmpleado() {
        return listaEmpleado;
    }
    

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }


    public void setDisComponents(DisabledComponents disComponents) {
        this.disComponents = disComponents;
    }

    public DisabledComponents getDisComponents() {
        return disComponents;
    }

    public void setComponentVisible(ShowHideComponents componentVisible) {
        this.componentVisible = componentVisible;
    }

    public ShowHideComponents getComponentVisible() {
        return componentVisible;
    }


    public void setShowHideButtons(ShowHideComponents showHideButtons) {
        this.showHideButtons = showHideButtons;
    }

    public ShowHideComponents getShowHideButtons() {
        return showHideButtons;
    }

    //Metodos Listeners
    
    public void dimensionarLista(ActionEvent event){
        this.size = this.listaEmpleado.size();
    }


    public void deshabilitarComponentes(ValueChangeEvent valueChangeEvent) {
        this.componentVisible.setStatus((Boolean) valueChangeEvent.getNewValue());
    }
    

    public void showHideButtons(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        this.showHideButtons.setStatus((Boolean) valueChangeEvent.getNewValue());
    }
    
    public void recibeEventoContextual(Object o){
        this.size = (Integer) o;
        
        JSFUtil.refreshComponent(JSFUtil.findComponentInRoot("ot3"));
    }
    
    
    public void disabledTable(ActionEvent event){
        
        this.disComponents.setButtons(Boolean.TRUE);
        this.disComponents.setColumns(Boolean.TRUE);
        this.disComponents.setTable(Boolean.TRUE);
        
    }
    
}
