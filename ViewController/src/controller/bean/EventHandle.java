package controller.bean;

import javax.faces.event.ActionEvent;

import libraries.*;

public class EventHandle {
    public EventHandle() {
        super();
    }
    
    public void proccessEvent(Object payload){
        IndexBean bean = (IndexBean) JSFUtil.resolveExpression("#{pageFlowScope.IndexBean}");
        bean.recibeEventoContextual(payload);      
    }
    
}
