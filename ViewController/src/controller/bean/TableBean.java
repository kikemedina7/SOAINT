package controller.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Locale;

import javax.faces.event.ActionEvent;

import model.*;

import libraries.*;

import services.EmpleadoFacade;

import tools.ShowHideComponents;

public class TableBean {
    private List<Empleado> listaEmpleado;
    private int searchId;
    private ShowHideComponents columnDelete;
    private ShowHideComponents buttonsHeader;
    private Empleado empleado;
    private DisabledComponents disComponents;

    public TableBean() {
        this.listaEmpleado = (List<Empleado>) JSFUtil.resolveExpression("#{pageFlowScope.ListaEmpleado}");
        this.columnDelete = (ShowHideComponents) JSFUtil.resolveExpression("#{pageFlowScope.ShowHide}");
        this.buttonsHeader = (ShowHideComponents) JSFUtil.resolveExpression("#{pageFlowScope.ShowHideButtons}");
        this.disComponents = (DisabledComponents) JSFUtil.resolveExpression("#{pageFlowScope.disabledElements}");
    }

    public void instanciarCableado(ActionEvent event) {

        if (!this.listaEmpleado.isEmpty()) {
            this.listaEmpleado.clear();
        }

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        try {
            this.listaEmpleado.add(new Empleado(123, "Jorge Enrique", "Medina Bencomo", 23,
                                                format.parse("27-04-1994")));
            this.listaEmpleado.add(new Empleado(123456, "Eugenio Enrique", "Ravelo Bencomo", 43,
                                                format.parse("11-08-1974")));
            this.listaEmpleado.add(new Empleado(234, "Isabel Cristina", "Bencomo", 63, format.parse("06-08-1954")));
            this.listaEmpleado.add(new Empleado(12, "Jorge Miguel", "Medina Gutierrez", 70,
                                                format.parse("17-11-1947")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void setListaEmpleado(List<Empleado> listaEmpleado) {
        this.listaEmpleado = listaEmpleado;
    }

    public List<Empleado> getListaEmpleado() {
        return listaEmpleado;
    }


    public void setSearchId(int searchId) {
        this.searchId = searchId;
    }

    public int getSearchId() {
        return searchId;
    }


    public void setColumnDelete(ShowHideComponents columnDelete) {
        this.columnDelete = columnDelete;
    }

    public ShowHideComponents getColumnDelete() {
        return columnDelete;
    }


    public void setButtonsHeader(ShowHideComponents buttonsHeader) {
        this.buttonsHeader = buttonsHeader;
    }

    public ShowHideComponents getButtonsHeader() {
        return buttonsHeader;
    }


    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Empleado getEmpleado() {
        return empleado;
    }


    public void setDisComponents(DisabledComponents disComponents) {
        this.disComponents = disComponents;
    }

    public DisabledComponents getDisComponents() {
        return disComponents;
    }


    public void setDataWs(ActionEvent event) {
        System.out.println("serach id" + this.searchId);
        String msg = EmpleadoFacade.getEmpleadoWs(listaEmpleado, this.searchId);
        JSFUtil.addFacesInformationMessage(msg);
    }

    public void eliminar(ActionEvent event) {
        int id = (Integer) event.getComponent().getAttributes().get("eliminarEmpleado");
        System.out.println("**************" + id);
        Iterator it = listaEmpleado.iterator();

        for (int i = 0; i < listaEmpleado.size(); i++) {
            Empleado em = (Empleado) it.next();
            if (em.getNumeroIdentificacion() == id) {
                listaEmpleado.remove(i);
            }
        }

    }
    
    
//    Eventos Contextuales


    public void setDynamicSize(){     
        ADFUtil.dispararContextualEvent("enviaEventBinding", listaEmpleado.size());       
    }

    public void buscarEmpleado(ActionEvent event) {
        Empleado empleado = (Empleado) event.getComponent().getAttributes().get("empleado");
        this.empleado = empleado;
    }
    

}
