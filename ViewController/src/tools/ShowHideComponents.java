package tools;

public class ShowHideComponents {
    
    private boolean status;
    
    public ShowHideComponents() {
        super();
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return status;
    }
}
