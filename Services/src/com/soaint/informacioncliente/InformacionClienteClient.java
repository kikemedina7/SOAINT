package com.soaint.informacioncliente;
// This source file is generated by Oracle tools.
// Contents may be subject to change.
// For reporting problems, use the following:
// Generated by Oracle JDeveloper 12c Development Build 12.2.1.0.0.1157

import java.util.Iterator;
public class InformacionClienteClient {
    public static void main(String[] args) {
        ServicioInformacionCliente servicioInformacionCliente = new ServicioInformacionCliente();
        InformacionClientePort informacionClientePort = servicioInformacionCliente.getInformacionCliente();
        // Add your code to call the desired methods.
        ClienteType list = new ClienteType();
        
        ListaCLienteType ty = informacionClientePort.consultarClientes(list);
        Iterator<ClienteType> it = ty.getListadoClientes().iterator();
        
        while(it.hasNext()){
            ClienteType cliente = it.next();
            System.out.println(cliente.getNombres()+ " " );
        }
    
    }
}
