package services;

import com.soaint.informacioncliente.ClienteType;
import com.soaint.informacioncliente.InformacionClientePort;
import com.soaint.informacioncliente.ListaCLienteType;
import com.soaint.informacioncliente.ServicioInformacionCliente;

import java.util.*;

import model.Empleado;
import model.Message;

public class EmpleadoFacade {
    
    public EmpleadoFacade() {
        super();
    }
    
    public static String getEmpleadoWs(List<Empleado> empleado,Integer idSearchEmpleado){
        ServicioInformacionCliente servicioInformacionCliente = new ServicioInformacionCliente();
        InformacionClientePort informacionClientePort = servicioInformacionCliente.getInformacionCliente();
        // Add your code to call the desired methods.
        ClienteType list = new ClienteType();
        
        ListaCLienteType ty = informacionClientePort.consultarClientes(list);
        Iterator<ClienteType> it = ty.getListadoClientes().iterator();
        
//      Borrando la lista
        empleado.clear();
        
        
        while(it.hasNext()){
            ClienteType cliente = it.next();
        //  System.out.println(cliente.getNombres()+ " " );
            if(!idSearchEmpleado.equals(0) && !idSearchEmpleado.equals(null)){
                if(cliente.getNumeroIdentificacion() == idSearchEmpleado){
                    empleado.add(new Empleado(cliente.getNumeroIdentificacion(), 
                                              cliente.getNombres(), 
                                              cliente.getApellidos(), 
                                              cliente.getEdad(), 
                                              (Date)cliente.getFechaRegistro().toGregorianCalendar().getTime())); 
                }
            }else{
                empleado.add(new Empleado(cliente.getNumeroIdentificacion(), 
                                          cliente.getNombres(), 
                                          cliente.getApellidos(), 
                                          cliente.getEdad(), 
                                          (Date)cliente.getFechaRegistro().toGregorianCalendar().getTime()));       
            }
        }
        
        if(empleado.isEmpty()){
            return Message.messageSuccess();
        }else{
            return Message.messageSuccess();
        }

    }
    
//    public static void main(String args[]){
//        List<Empleado> empleado = new ArrayList<Empleado>();
//        System.out.println(EmpleadoFacade.getEmpleadoWs(empleado, 3));
//    }
    
}
